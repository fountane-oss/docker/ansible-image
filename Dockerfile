# docker rmi testing:latest
# docker build -f ./Dockerfile . -t testing:latest

FROM ubuntu:bionic

WORKDIR /app

RUN apt-get update
# RUN apt-get upgrade -y
RUN apt-get install software-properties-common -y
RUN apt-add-repository ppa:ansible/ansible
RUN apt-get update
RUN apt-get install ansible -y
RUN apt-get install python -y

